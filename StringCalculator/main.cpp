#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "StringCalculator.h"

using Catch::Matchers::Contains;


TEST_CASE("empty string returns 0") {
	REQUIRE(StringCalculator::add("") == 0);
}

TEST_CASE("single number is returned") {
	REQUIRE(StringCalculator::add("1") == 1);
}

TEST_CASE("multiple arguments are added") {
	REQUIRE(StringCalculator::add("1,1") == 2);
}

TEST_CASE("line break can be used as delimiter") {
	REQUIRE(StringCalculator::add("1\n2") == 3);
}

TEST_CASE("delimiter can be configured in first line") {
	REQUIRE(StringCalculator::add("//x\n1x3") == 4);
}

TEST_CASE("negative numbers are not allowed") {
	REQUIRE_THROWS_WITH(StringCalculator::add("-1"), Contains("negatives not allowed: -1"));
}

TEST_CASE("all negatives are listed in case of error") {
	REQUIRE_THROWS_WITH(StringCalculator::add("-1,-2"), Contains("negatives not allowed: -1 -2"));
}

