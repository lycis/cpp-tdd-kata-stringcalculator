#pragma once

#include <string>
#include <algorithm>
#include <vector>

using NumberList = std::vector<int>;

class StringCalculator {
public:
	static auto add(std::string input) -> int;

private:
	static auto splitStringToNumbers(const std::string input, const std::string delimiter) -> NumberList;
	static auto extractNumbersFromString(const std::string input, const std::string delimiter) -> NumberList;
	static auto throwExceptionForNegativeNumbers(NumberList& negatives) -> void;
	static auto positionAt(size_t& position, std::string& remaining) -> void;
	static auto extractDelimiterIfGiven(std::string& input) -> std::string;
};