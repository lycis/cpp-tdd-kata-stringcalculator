#include "StringCalculator.h"

int StringCalculator::add(std::string input) {
	if (input.size() == 0) {
		return 0;
	}


	std::string delimiter = extractDelimiterIfGiven(input);
	NumberList numbers = splitStringToNumbers(input, delimiter);

	int result(0);
	std::for_each(numbers.begin(), numbers.end(), [&result](int n) { result += n;  });

	return result;
}

std::string StringCalculator::extractDelimiterIfGiven(std::string& input)
{
	std::string delimiter = ",\n";

	if (input.substr(0, 2) == "//") {
		size_t positionOfEol = input.find("\n");
		delimiter = input.substr(2, positionOfEol - 2);
		input = input.substr(positionOfEol + 1);
	}

	return delimiter;
}

NumberList  StringCalculator::splitStringToNumbers(const std::string input, const std::string delimiter)
{
	NumberList numbers = extractNumbersFromString(input, delimiter);

	return numbers;
}

NumberList StringCalculator::extractNumbersFromString(const std::string input, const std::string delimiter)
{
	NumberList numbers;
	std::string remaining(input);
	
	bool lastElement = false;
	NumberList negatives;
	while (!lastElement) {
		size_t position = remaining.find_first_of(delimiter);

		if (position == std::string::npos) {
			position = remaining.size();
			lastElement = true;
		}

		std::string numberStr = remaining.substr(0, position);
		int number = std::stoi(numberStr);
		if (number < 0) {
			negatives.push_back(number);
		}

		numbers.push_back(number);

		if (!lastElement) {
			positionAt(position, remaining);
		}
	}

	if (negatives.size() > 0) {
		throwExceptionForNegativeNumbers(negatives);
	}

	return numbers;
}

void StringCalculator::throwExceptionForNegativeNumbers(NumberList& negatives)
{
	std::string message = "negatives not allowed:";
	std::for_each(negatives.begin(), negatives.end(), [&message](int n) {
		message.append(" " + std::to_string(n));
		});
	throw message;
}

void  StringCalculator::positionAt(size_t& position, std::string& remaining)
{
	position += 1;
	if (position > remaining.size()) {
		remaining = "";
	}
	else {
		remaining = remaining.substr(position);
	}
}
